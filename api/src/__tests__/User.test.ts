import request from 'supertest';
import {app} from '../app';

import createConnection from '../database';

describe("Users", () => {
  beforeAll(async() => {
    const connection = await createConnection();
    await connection.runMigrations();
  })

  it("Shold be able to create a new user", async () =>{
    const response = request(app).post("/users").send({
    email: "user@example.com",
    name: "user Example"
  });

  expect((await response).status).toBe(201);
  });
});