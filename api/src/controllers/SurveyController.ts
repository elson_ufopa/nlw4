import { getCustomRepository } from 'typeorm';
import { Request, response, Response} from "express";
import { SurveysRepository } from '../repositories/SurveyRepository';
import { Survey } from '../models/Survey';

class SurveysController {

  async create(req: Request, res: Response) {
    const {title, description} = req.body;

    const surveysRepository = getCustomRepository(SurveysRepository);

    const survey = surveysRepository.create({
      title,
      description,
    });

    await surveysRepository.save(survey);

    return res.status(201).json(survey);
  }

  //Criar um metodo para listar as pesquisas 

  async show( req:Request, res:Response){
    const surveysRepository = getCustomRepository(SurveysRepository);

    const all = await surveysRepository.find();

    return res.json(all);
  }
  
}
export {SurveysController}