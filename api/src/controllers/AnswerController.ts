import { Request, Response } from 'express';
import { getCustomRepository } from 'typeorm';
import { SurveysUsersRepository } from '../repositories/SurveysUsersRepository';

/**
 * 
 * Router Params => Parametros que compoe a rota "/"
 * router.get("/answer/:value/:nota/:batata")
 * 
 * Query Params => Busca, Paginacao, nao obrigatorios
 * ?
 * chave = valor
 */

class AnswerController {

  async execute(req: Request, res: Response) {
    const { value } = req.params;
    const { u } = req.query;

    const surveysUsersRepository = getCustomRepository(SurveysUsersRepository);

    const surveyUser = await surveysUsersRepository.findOne({
      id: String(u)
    });

    if(!surveyUser) {
      return res.status(400).json({
        error: "Survey User does not exists!"
      })
    }
    
    surveyUser.value = Number(value);

    await surveysUsersRepository.save(surveyUser);

    return res.json(surveyUser);
  }
}

export {AnswerController}